/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
public class Main
{

  public static void main (String[]args)
  {

    int n = 50, i = 0, j = 1;


      System.out.println ("Fibonacci Series Upto " + n + ": ");

    while (i <= n)
      {
	System.out.println (i);

	int k = i + j;
	  i = j;
	  j = k;

      }
  }
}
